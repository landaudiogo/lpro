import java.util.Scanner;
import java.util.Random; 
import java.lang.Math;
import java.io.IOException;

public class Quest1 
{
	
//	public class map_and_pos
//	{
//		int[] key_pos;
//		int[] exit_pos;
//		int[] dragon_pos;
//		int[] player_pos;
//		String[][] game_map;
//	}
	
	public static int getRandomIntegerBetweenRange(int min, int max){
	    int x = (int)(Math.random()*((max-min)+1))+min;
	    return x;
	}
	
	public static String[][] initialize(String[][] game_map)
	{
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10; j++)
			{
				game_map[i][j] = " ";
			}
		}
		
		return game_map;
	}
	
	public static void draw_map(String[][] game_map)
	{
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10; j++)
			{	
				System.out.print(game_map[i][j] + " ");	
			}
			System.out.print("\n");
		}
	}

	
	public static void main(String[] args)
	{
		String[][] game_map= new String[10][10];
		String s;
		int[] player_pos = new int[2];
		int[] dragon_pos = new int[2];
		int[] key_pos = new int[2];
		int[] exit_pos = new int[2];
		boolean key_flag = false;
		boolean win_flag = false;
		boolean lose_flag = false;
		int[] random= new int[2];

		
		//introduce " " in the map (empty map)
		game_map = initialize(game_map);
		
		//store loose values
		game_map[8][2] ="x";
		game_map[8][3] ="x";
		game_map[5][7] ="x";
		game_map[5][9] ="x";
		
		//store outer walls
		for(int i=0; i<10; i++)
		{
			game_map[0][i] = "x";
			game_map[9][i] = "x";
		}

		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10; j++)
			{
				if( (j==0 || j==9) && !(i==5 && j==9) )
				{
					game_map[i][j]= "x";
				}
			}
		}
		
		//store inner walls
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10;j++)
			{
				if( ( (i>1 && i<5) || (i==6 || i==7) ) && (j==2 || j==3 || j==5 || j==7) )
				{
					game_map[i][j]= "x";
				}
			}
		}
		
		////////////////////////////////////////
		//////// map is designed ///////////////
		////////////////////////////////////////
		
		//randomly place rest of elements
		
		//define player position
		random[0] = getRandomIntegerBetweenRange(1,8);
		random[1] = getRandomIntegerBetweenRange(1,8);
		
		while(true)
		{
			if( !game_map[ random[1] ][ random[0] ].equals("x") )
			{
				player_pos[1] = random[1];
				player_pos[0] = random[0];
				game_map[ player_pos[1] ][ player_pos[0] ] = "H";
				break;
			}
			random[0] = getRandomIntegerBetweenRange(1,8);
			random[1] = getRandomIntegerBetweenRange(1,8);
		}
		
		//define key position
		random[0] = getRandomIntegerBetweenRange(1,8);
		random[1] = getRandomIntegerBetweenRange(1,8);
		
		while(true)
		{
			if( !( game_map[ random[1] ][ random[0] ].equals("x") || game_map[ random[1] ][ random[0] ].equals("H") ) )
			{
				key_pos[1] = random[1];
				key_pos[0] = random[0];
				game_map[ key_pos[1] ][ key_pos[0] ] = "K"; 
				break;
			}
			random[0] = getRandomIntegerBetweenRange(1,8);
			random[1] = getRandomIntegerBetweenRange(1,8);
		}
		
		
		//define Dragon position
		random[0] = getRandomIntegerBetweenRange(1,8);
		random[1] = getRandomIntegerBetweenRange(1,8);
		
		while(true)
		{
			if( !( game_map[ random[1] ][ random[0] ].equals("x") || game_map[ random[1] ][ random[0] ].equals("H") || game_map[ random[1] ][ random[0] ].equals("K") ) && !( ( random[0]-player_pos[0]==1 && random[1]==player_pos[1] ) || ( random[0]-player_pos[0]==-1 && random[1]==player_pos[1] ) || ( random[0]-key_pos[0]==1 && random[1]==key_pos[1] ) || ( random[0]-key_pos[0]==-1 && random[1]==key_pos[1] ) || ( random[1]-player_pos[1]==1 && random[0]==player_pos[0] ) || ( random[1]-player_pos[1]==-1 && random[0]==player_pos[0] ) || ( random[1]-key_pos[1]==1 && random[0]==key_pos[0] ) || ( random[1]-key_pos[1]==-1 && random[0]==key_pos[0] ) ) )
			{
				dragon_pos[1] = random[1];
				dragon_pos[0] = random[0];
				game_map[ dragon_pos[1] ][ dragon_pos[0] ] = "D"; 
				break;
			}
			random[0] = getRandomIntegerBetweenRange(1,8);
			random[1] = getRandomIntegerBetweenRange(1,8);
		}
		
		//define exit position
		random[0] = getRandomIntegerBetweenRange(1,4);
		random[1] = getRandomIntegerBetweenRange(1,8);
		
		while(true)
		{
			if(random[0]==1)
			{
				game_map[ random[1] ][0] ="E";
				break;
			}
			else if(random[0]==2)
			{
				game_map[0][ random[1] ]="E";
				break;
			}
			else if(random[0]==3)
			{
				game_map[ random[1] ][ 9 ]="E";
				break;
			}
			else if(random[0]==4 && random[1]!=2 && random[1]!=3)
			{
				game_map[9][ random[1] ]="E";
				break;
			}
			random[0] = getRandomIntegerBetweenRange(1,4);
			random[1] = getRandomIntegerBetweenRange(1,8);
		}

		
		
		//Draw out first map
		draw_map(game_map);
		
		Scanner scanIn = new Scanner(System.in);
		
		while(true)
		{
			s = scanIn.nextLine();
			
			
			
			
			
			
			if( s.equals("exit") ) 
			{
				break;
			}
			else if( s.equals("w") && game_map[ player_pos[1]-1 ][ player_pos[0] ].equals("E") && !key_flag)
			{
				System.out.print("You need the key first \n");
			}
			else if( s.equals("a") && game_map[ player_pos[1] ][ player_pos[0]-1 ].equals("E") && !key_flag)
			{
				System.out.print("You need the key first \n");
			}
			else if( s.equals("s") && game_map[ player_pos[1]+1 ][ player_pos[0] ].equals("E") && !key_flag)
			{
				System.out.print("You need the key first \n");
			}
			else if( s.equals("d") && game_map[ player_pos[1] ][ player_pos[0]+1 ].equals("E") && !key_flag)
			{
				System.out.print("You need the key first \n");
			}
			else if( s.equals("w") && !game_map[ player_pos[1]-1 ][ player_pos[0] ].equals("x") && player_pos[1] > 0 )
			{
				game_map[player_pos[1]][player_pos[0]] = " ";
				player_pos[1]--;
			}
			else if( s.equals("s") && !game_map[ player_pos[1]+1 ][ player_pos[0] ].equals("x") && player_pos[1]<9 )
			{
				game_map[player_pos[1]][player_pos[0]] = " ";
				player_pos[1]++;
			}
			else if( s.equals("d") && !game_map[ player_pos[1] ][ player_pos[0]+1 ].equals("x") && player_pos[0]<9 )
			{
				game_map[player_pos[1]][player_pos[0]] = " ";
				player_pos[0]++;
			}
			else if( s.equals("a") && !game_map[ player_pos[1] ][ player_pos[0]-1 ].equals("x") && player_pos[0]>0)
			{
				game_map[player_pos[1]][player_pos[0]] = " ";
				player_pos[0]--;
			}
			
			
//move dragon randomly
			
			int aleat = getRandomIntegerBetweenRange(1, 4);
			while(true)
			{
				if(aleat ==1 && !(game_map[ dragon_pos[1]-1 ][ dragon_pos[0] ].equals("x") || game_map[ dragon_pos[1]-1 ][ dragon_pos[0] ].equals("E") || game_map[ dragon_pos[1]-1 ][ dragon_pos[0] ].equals("K") ) )
				{
					game_map[ dragon_pos[1] ][ dragon_pos[0] ] = " ";
					dragon_pos[1]--;
					break;
				}
				else if(aleat==2 && !(game_map[ dragon_pos[1] ][ dragon_pos[0]-1 ].equals("x") || game_map[ dragon_pos[1] ][ dragon_pos[0]-1 ].equals("E") || game_map[ dragon_pos[1] ][ dragon_pos[0]-1 ].equals("K") ))
				{
					game_map[ dragon_pos[1] ][ dragon_pos[0] ] = " ";
					dragon_pos[0]--;
					break;
				}
				else if(aleat ==3 && !(game_map[ dragon_pos[1]+1 ][ dragon_pos[0] ].equals("x") || game_map[ dragon_pos[1]+1 ][ dragon_pos[0] ].equals("E") || game_map[ dragon_pos[1]+1 ][ dragon_pos[0] ].equals("K") ) )
				{
					game_map[ dragon_pos[1] ][ dragon_pos[0] ] = " ";
					dragon_pos[1]++;
					break;
				}
				else if(aleat ==4 && !(game_map[ dragon_pos[1] ][ dragon_pos[0]+1 ].equals("x") || game_map[ dragon_pos[1] ][ dragon_pos[0]+1 ].equals("E") || game_map[ dragon_pos[1] ][ dragon_pos[0]+1 ].equals("K") ))
				{
					game_map[ dragon_pos[1] ][ dragon_pos[0] ] = " ";
					dragon_pos[0]++;
					break;
				}
				aleat = getRandomIntegerBetweenRange(1, 4);
			}
			
			
			if( game_map[ player_pos[1] ][ player_pos[0] ].equals("K") )
			{
				System.out.print("You have retrieved the key \n");
				key_flag = true;
			}
			if( game_map[ player_pos[1] ][ player_pos[0] ].equals("E"))
			{
				win_flag = true;
			}
			if( ( (dragon_pos[0]-player_pos[0])==1 || (dragon_pos[0]-player_pos[0])==-1 ) && dragon_pos[1]==player_pos[1] )
			{
				lose_flag= true;
			}
			if( ( (dragon_pos[1]-player_pos[1])==1 || (dragon_pos[1]-player_pos[1])==-1 ) && dragon_pos[0]==player_pos[0] )
			{
				lose_flag=true;
			}
			if( (dragon_pos[1]-player_pos[1]==0) && (dragon_pos[0]-player_pos[0]==0) )
			{
				lose_flag=true;
				
			}
			game_map[player_pos[1]][player_pos[0]] = "H";
			game_map[dragon_pos[1]][dragon_pos[0]] = "D";
			
			
			/////////////////////////
			////player input done////
			/////////////////////////
			
			

			
			// print new map with new pos
			for(int i=0; i<10; i++)
			{
				for(int j=0; j<10; j++)
				{	
					System.out.print(game_map[i][j] + " ");	
				}
				System.out.print("\n");
			}
			
			//checking positions
//			System.out.print(dragon_pos[0] + " ");
//			System.out.print(dragon_pos[1] + "\n");
//			System.out.print(player_pos[0] + " ");
//			System.out.print(player_pos[1] + "\n");			
			
			
			if(win_flag)
			{
				System.out.print("You have won\n");
				break;
			}
			if(lose_flag)
			{
				System.out.print("You have lost\n");
				break;
			}
			
		
		
		}
	
	}
	
	
	
	

}
